## tipro ortho ansi

  ![tipro 128 8x16](pics/tipro/tiproortho-ansi.webp)

### introduction

this is an ortho ansi combination from tipro that I have converted to use qmk using same pcb as [here](https://mlego.elena.space/tipro8x16/)

this is in working, page updated once finished.


### wiring

![m128 ortho ansi ](pics/tipro/tipro_orthoansi_bottom.webp)

### firmware

### other pictures

original

![m128 ortho ansi ](pics/tipro/tipro-ortho-ansi-original.webp)

cleaned

![m128 ortho ansi ](pics/tipro/tipro-ortho-ansi-v1.webp)

switches

![m120 ortho ansi](pics/tipro/tipro-ortho-ansi-v2.webp)

