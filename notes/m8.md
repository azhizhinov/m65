### m8 2x4 in lego

![real thing](https://gitlab.com/m-lego/m4x2/-/raw/main/pics/m4x2.jpg)

this is another test board, shows a simple circuitry how to use a matrix scanned with hc595, hc589 and eink (spi screen), i2c screen
and clickable encoder.

####  kicad
the project is available in here

https://gitlab.com/m-lego/m4x2
